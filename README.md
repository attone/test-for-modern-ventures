# Function TestCase:

## Input data:

```
A: bool
B: bool
C: bool
D: float
E: int
F: int
```

## Base rules:

```
A && B && !C => H = M
A && B && C => H = P
!A && B && C => H = T
[other] => [error]
H = M => K = D + (D * E / 10)
H = P => K = D + (D * (E - F) / 25.5)
H = T => K = D - (D * F / 30)
```

### Default usage:

```js
testCase(true, true, false, 12.7, 13, 7);

/*
{
  h: "p"
  k: 28.49411764705882
  custom1: [Object]
  custom2: [Object]
}
*/
```

## Additional usage:

### Custom1:

#### Rules:

```
H = P => K = 2 * D + (D * E / 100)
```

#### Usage:
```js
testCase(true, true, false, 12.7, 13, 7).custom1;

/*
{
  h: "p"
  k: 44.625
}
*/
```


### Custom2:

#### Rules:

```
A && B && !C => H = T
A && !B && C => H = M
H = M => K = F + D + (D * E / 100)
```

#### Usage:
```js
testCase(true, true, false, 12.7, 13, 7).custom2;

/*
{
  h: "t"
  k: 18.62
}
*/
```
