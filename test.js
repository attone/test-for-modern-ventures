function TestCase(a, b, c, d, e, f) {
  const m = 'm';
  const p = 'p';
  const t = 't';

  const getCaseOne = a && b && !c;
  const getCaseTwo = a && b && c;
  const getCaseThree = !a && b && c;

  function Base(type = null) {
    let result = 0;

    if (getCaseOne && !type) {
      type = m;
    } else if (getCaseTwo && !type) {
      type = p;
    } else if (getCaseThree && !type) {
      type = t;
    } else if (!type) {
      throw new Error('wrong data');
    }
  
    if (type === m) {
      result = d + (d * e / 10);
    } else if (type === p) {
      result = d + (d * (e - f) / 25.5);
    } else if (type === t) {
      result = d - (d * f / 30);
    }

    this.h = type;
    this.k = result;
  }

  const custom1 = function() {
    let result;
    const base = new Base();

    if (base.h === p) {
      result = 2 * d + (d * e / 100);
    } else {
      result = base.k;
    }

    return {
      h: base.h,
      k: result,
    };
  }

  const custom2 = () => {
    let result;
    let type;

    if (a && b && !c) {
      type = t;
    } else if (a && !b && c) {
      type = m;
    }

    const base = new Base(type);

    if (type === m) {
      result = f + d + (d * e / 100);
    } else {
      result = base.k;
    }

    return {
      h: base.h,
      k: result,
    }
  }

  const base = new Base();

  return {
    h: base.h,
    k: base.k,
    custom1: custom1(),
    custom2: custom2(),
  };
}
